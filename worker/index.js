const fetchGithubJobs = require('./tasks/fetch-github');
var CronJob = require('cron').CronJob;
var job = new CronJob('* * * * * ', fetchGithubJobs, null, true, 'America/Los_Angeles');
job.start();