import React, { useState, useEffect } from "react";
import Job from "./job";

import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import JobPagination from "./JobPagination";


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '80%',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));

//import AutoGridNoWrap from "./job";

const mockJobs = [
    {
        jobid: 123,
        title: "Software Developer",
        description: "Javascript enginner requried."
    },
    {
        jobid: 123,
        title: "Software Developer",
        description: "Javascript enginner requried."
    },
    {
        jobid: 123,
        title: "Software Developer",
        description: "Javascript enginner requried."
    }
];
const baseURL = `http://localhost:3001`;

function JobList() {
    const classes = useStyles();
    const [allJobs, setAllJobs] = useState([]);
    const [pageinationConfig, setPaginationConfig] = useState({ offset: 0, perPage: 20, currentPage: 0 });
    const [totalPages, setTotalPages] = useState(1);
    const [displayJobs, setDisplayJobs] = useState([]);



    const loadJob = async () => {
        const res = await fetch(`${baseURL}/jobs`);
        const jobs = await res.json();
        // console.log(jobs);
        setAllJobs(jobs || []);
        const countPages = Math.ceil(jobs.length / pageinationConfig.perPage);
        console.log(countPages);
        setTotalPages(countPages - 1);
        setDisplayJobs(jobs.slice(0, pageinationConfig.perPage));
    }

    useEffect(() => {
        loadJob()
    }, [])

    function calculatePagination(pageN) {

        const offset = pageN * pageinationConfig.perPage;
        console.log(offset);
        const slicedArr = allJobs.slice(offset, offset + pageinationConfig.perPage);
        console.log(slicedArr);
        setPaginationConfig({
            ...pageinationConfig,
            currentPage: pageN
        });
        setDisplayJobs(slicedArr);
    }

    function pageChanged(evt, pageN) {
        console.log(evt, pageN)
        calculatePagination(pageN);
    }

    return (<div>
        <List className={classes.root}>
            {/* <AutoGridNoWrap/> */}
            {
                displayJobs.map((job, idx) => {
                    return <Job key={`${job.id}-$${idx}`} info={job} />
                })
            }
        </List>
        <div>
            {
                allJobs.length ? <JobPagination totalPageCount={totalPages} currentPage={pageinationConfig.currentPage} onPageChanged={pageChanged} /> : null
            }
        </div>
    </div>)
}
export default JobList;