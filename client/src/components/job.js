import React,{useState} from 'react';
import parse from 'html-react-parser';
import { makeStyles } from '@material-ui/core/styles';
// import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import LocationOnIcon from '@material-ui/icons/LocationOn';
// import ArrowRightIcon from '@material-ui/icons/ArrowRight';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '80%',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
}));

export default function Job({info}) {
  const classes = useStyles();
  const [showMoreDetails,setShowMoreDetails] =useState(false);

  return (
    <React.Fragment>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Remy Sharp" src={info.company_logo ||   "/static/images/avatar/1.jpg" }   />
        </ListItemAvatar>
        <ListItemText
          primary={info.title || 'JD'}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
              {info.type || 'N/A' }
              </Typography>

              <Typography
                component="div"
              //  variant="body2"
               // className={classes.inline}
                color="textPrimary"
              >
                 {info.location || 'N/A' }  <LocationOnIcon/> 
              </Typography>

              <Typography
                component="div"
              //  variant="body2"
               // className={classes.inline}
                color="textPrimary"
              >
                 {info.created_at || 'N/A' } 
              </Typography>
              <div>
                <div  className={classes.inline}>
                  <span onClick={()=>setShowMoreDetails(!showMoreDetails)}>
                 { !showMoreDetails ? 'see more' : 'see less'}  
                  </span>
                  
                </div>
              {

                showMoreDetails ?  parse(info.description || '') : null
                
            }
              </div>
            
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      </React.Fragment>
  );
}
